# Advent of Code 2022

Rust solutions for [Advent of Code 2022](https://adventofcode.com/2022)

## Prerequisites

To run solutions, you'll need the following commands available
- Cargo [(installation)](https://rustup.rs/)
- Just [(installation)](https://just.systems/man/en/)

## Usage

```bash
just run [day] # day is a number in range (1..=25)
```