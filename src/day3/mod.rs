use itertools::Itertools;
use std::collections::HashSet;

const INPUT: &'static str = include_str!("input.in");

fn get_value(s: String) -> u32 {
    match s.chars().next() {
        Some(char @ 'a'..='z') => (char as u32 - 'a' as u32) + 1,
        Some(char @ 'A'..='Z') => (char as u32 - 'A' as u32) + 27,
        _ => panic!("This isn't supposed to happen"),
    }
}

pub fn part_1() {
    let solution: u32 = INPUT
        .lines()
        .flat_map(|line| {
            let (first, second) = line.split_at(line.len() / 2);
            vec![first, second]
        })
        .map(|pack| pack.chars().collect::<HashSet<char>>())
        .tuples()
        .map(|(first, second)| get_value(first.intersection(&second).collect()))
        .sum();

    println!("Part 1 Solution: {:?}", solution)
}

pub fn part_2() {
    let solution: u32 = INPUT
        .lines()
        .map(|line| line.chars().collect::<HashSet<char>>())
        .tuples()
        .map(|(elf1, elf2, elf3)| {
            let badge: String = elf1
                .iter()
                .filter(move |i| elf2.contains(i))
                .filter(move |i| elf3.contains(i))
                .collect::<String>();

            get_value(badge)
        })
        .sum();

    println!("Part 2 Solution: {:?}", solution)
}
