use std::cmp;

const INPUT: &'static str = include_str!("input.in");

fn parse_range(s: &str) -> (u32, u32) {
    s.split_once("-")
        .map(|(min, max)| (min.parse::<u32>().unwrap(), max.parse::<u32>().unwrap()))
        .unwrap()
}

pub fn part_1() {
    let solution = INPUT
        .lines()
        .filter(|line| {
            let ((min1, max1), (min2, max2)) = line
                .split_once(",")
                .map(|(first, second)| (parse_range(first), parse_range(second)))
                .unwrap();

            (min1 <= min2 && max1 >= max2) || (min2 <= min1 && max2 >= max1)
        })
        .count();
    println!("Part 1 Solution: {:?}", solution);
}

pub fn part_2() {
    let solution = INPUT
        .lines()
        .filter(|line| {
            let ((min1, max1), (min2, max2)) = line
                .split_once(",")
                .map(|(first, second)| (parse_range(first), parse_range(second)))
                .unwrap();

            cmp::max(max1, max2) - cmp::min(min1, min2) <= (max1 - min1) + (max2 - min2)
        })
        .count();
    println!("Part 2 Solution: {:?}", solution);
}
