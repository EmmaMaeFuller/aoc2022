use itertools::Itertools;
use std::ops::Add;

fn get_calories_list() -> std::vec::IntoIter<u32> {
    include_str!("input.in")
        .split("\n\n")
        .flat_map(|elf| elf.lines().map(str::parse::<u32>).fold_ok(0, Add::add))
        .sorted_by(|a, b| Ord::cmp(&b, &a))
}

pub fn part_1() {
    println!("Day 1 Part 1 Solution: {:?}", get_calories_list().next());
}

pub fn part_2() {
    println!(
        "Day 1 Part 1 Solution: {:?}",
        get_calories_list().next_tuple().map(|(a, b, c)| a + b + c)
    );
}
