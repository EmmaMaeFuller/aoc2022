const INPUT: &'static str = include_str!("input.in");

pub fn part_1() {
    let solution = INPUT.lines().fold(0, |acc, line| {
        if let Some((theirs, yours)) = line.split_once(" ") {
            let hand_value = match yours {
                "X" => 1,
                "Y" => 2,
                "Z" => 3,
                _ => panic!("Malformed input file"),
            };

            let outcome_value = match (theirs, yours) {
                ("B", "X") | ("C", "Y") | ("A", "Z") => 0,
                ("A", "X") | ("B", "Y") | ("C", "Z") => 3,
                ("C", "X") | ("A", "Y") | ("B", "Z") => 6,
                _ => panic!("Malformed input file"),
            };

            acc + hand_value + outcome_value
        } else {
            acc
        }
    });

    println!("Part 1 Solution: {:?}", solution);
}

pub fn part_2() {
    let solution = INPUT.lines().fold(0, |acc, line| {
        if let Some((theirs, outcome)) = line.split_once(" ") {
            let outcome_value = match outcome {
                "X" => 0,
                "Y" => 3,
                "Z" => 6,
                _ => panic!("Malformed input file"),
            };

            let hand_value = match (theirs, outcome) {
                ("B", "X") | ("A", "Y") | ("C", "Z") => 1,
                ("C", "X") | ("B", "Y") | ("A", "Z") => 2,
                ("A", "X") | ("C", "Y") | ("B", "Z") => 3,
                _ => panic!("Malformed input file"),
            };

            acc + outcome_value + hand_value
        } else {
            acc
        }
    });

    println!("Part 2 Solution: {:?}", solution)
}
